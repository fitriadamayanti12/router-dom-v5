import React, { Component, Fragment } from "react";
import { withRouter } from "react-router";
import { Redirect } from "react-router-dom";
import { Button, Container } from "reactstrap";
import NavBar from "../components/NavBar";

class About extends Component {
  state = { isAuthenticated: false };
  componentWillMount() {
    this.checkUser();
  }

  // Ini nanti harusnya request beneran ke sever
  checkUser = () => {
    const token = localStorage.getItem("token");
    if (!!token) {
      // Cek token valid ke sever
      return this.setState({ isAutenticated: true });
    }
  };
  handleLogout = () => {
    const { history } = this.props;
    localStorage.removeItem("token");
    history.push("/login");
  };
  render() {
    const { isAutenticated } = this.state;
    if (!isAutenticated) return <Redirect to="/login" />;
    return (
      <Fragment>
        <NavBar />
        <Container>
          <h1 className="p-4">Ini halaman About</h1>
          <Button color="danger">Logout</Button>
        </Container>
      </Fragment>
    );
  }
}

export default withRouter(About);
